const express = require('express')
const google = require('googleapis').google
const youtube = google.youtube({ version: 'v3'})
const OAuth2 = google.auth.OAuth2
const state = require('./state')
const fs = require('fs')

async function robot() {
    const content = state.load()

    await authenticateWithOAuth()
    await uploadVideo(content)
    await uploadThumbnail()


    async function authenticateWithOAuth() {
        const webServer = await startWebServer()
        const OAuthClient = await createOAuthClient()

        requestUserConsent(OAuthClient)

        const authToken = await waitForGoogleCallback(webServer)

        await requestGoogleForAccessTokens(OAuthClient, authToken)
        await setGlobalGoogleAuthentication(OAuthClient)
        await stopWebServer(webServer)

        async function startWebServer() {
            return new Promise((res, rej) => {
                const port = process.env.PORT || 4554
                const app = express()

                const server = app.listen(port, () => {
                    console.log(`> [youtube-robot] Listening on http://localhost:${port}`)

                    res({ app, server})
                })
            })
        }

        async function createOAuthClient() {
            const credentials = require('../credentials/google-youtube.json')
        
            return new OAuth2(
                credentials.web.client_id,
                credentials.web.client_secret,
                credentials.web.redirect_uris[0]
            )
        }

        function requestUserConsent(OAuthClient) {
            const consentUrl = OAuthClient.generateAuthUrl({
                access_type: 'offline',
                scope: ['https://www.googleapis.com/auth/youtube']
            })

            console.log(`> [youtube-robot] Please give your consent: ${consentUrl}`)
        }

        async function waitForGoogleCallback(webServer) {
            return new Promise((res, rej) => {
                console.log('> [youtube-robot] Waiting for user consent...')
        
                webServer.app.get('/oauth', (req, resp) => {
                    const authCode = req.query.code
                    console.log(`> [youtube-robot] Consent given: ${authCode}`)
            
                    resp.send('<h1>Thank you!</h1><p>Now close this tab.</p>')
                    res(authCode)
                })
            })
        }

        async function requestGoogleForAccessTokens(OAuthClient, authorizationToken) {
            return new Promise((res, rej) => {
                OAuthClient.getToken(authorizationToken, (error, tokens) => {
                    if (error) { return rej(error) }
        
                    console.log('> [youtube-robot] Access tokens received!')
            
                    OAuthClient.setCredentials(tokens)
                    res()
                })
            })
        }

        function setGlobalGoogleAuthentication(OAuthClient) {
            google.options({
                auth: OAuthClient
            })
        }
        
        async function stopWebServer(webServer) {
            return new Promise((res, rej) => {
                webServer.server.close(() => {
                    res()
                })
            })
        }
        
    }

    async function uploadVideo(content) {
        const videoFilePath = './content/output.mov'
        const videoFileSize = fs.statSync(videoFilePath).size
        const videoTitle = `${content.prefix} ${content.searchTerm}`
        const videoTags = [content.searchTerm, ...content.sentences[Math.floor(Math.random() * content.sentences.length)].keywords]
        const videoDescription = content.sentences.map(sentence => sentence.join('\n\n'))

        const requestParameters = {
            part: 'snippet, status',
            requestBody: {
                snippet: {
                title: videoTitle,
                description: videoDescription,
                tags: videoTags
                },
                status: {
                    privacyStatus: 'unlisted'
                }
            },
            media: {
                body: fs.createReadStream(videoFilePath)
            }
        }

        console.log('> [youtube-robot] Starting to upload the video to YouTube')

        const youtubeResponse = await youtube.videos.insert(requestParameters, {
            onUploadProgress: onUploadProgress
        })

        console.log(`> [youtube-robot] Video available at: https://youtu.be/${youtubeResponse.data.id}`)
        return youtubeResponse.data

        function onUploadProgress(event) {
            const progress = Math.round( (event.bytesRead / videoFileSize) * 100 )
            console.log(`> [youtube-robot] ${progress}% completed`)
        }

    }

    async function uploadThumbnail(videoInformation) {
        const videoId = videoInformation.id
        const videoThumbnailFilePath = './content/youtube-thumbnail.jpg'

        const requestParameters = {
            videoId: videoId,
            media: {
                mimeType: 'image/jpeg',
                body: fs.createReadStream(videoThumbnailFilePath)
            }
        }

        await youtube.thumbnails.set(requestParameters)
        console.log(`> [youtube-robot] Thumbnail uploaded!`)
    }

}

module.exports = robot