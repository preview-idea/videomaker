const algorithmia = require('algorithmia')
const algorithmiaApiKey = require('../credentials/algorithmia.json').apiKey
const sbd = require('sbd')
const naturalLanguage = require('ibm-watson/natural-language-understanding/v1')
const state = require('../robots/state')

const nlu = new naturalLanguage({ version: '2019-06-29' })

async function robot () {
    const content = state.load()

    await fecthContentFromWikipedia(content)
    await sanitizeContent(content)
    await breakContentIntoSentence(content)
    await fetchKeywordsOfAllSentences(content)

    state.save(content)

    async function fecthContentFromWikipedia(content) {

        const wikipediaAlgorithm = await algorithmia.client(algorithmiaApiKey)
            .algo('web/WikipediaParser/0.1.2')
            .pipe({ 
                articleName: content.searchTerm, 
                lang: content.language 
            })    
        
        content.sourceContentOriginal = wikipediaAlgorithm.get().content.substr(0, content.maxResult * 550)
        console.log('> [text-robot] Fetching done!') 
    }

    function sanitizeContent(content) {
        const withoutBlankLinesAndMarkdown = removeBlankLines(content.sourceContentOriginal)

        function removeBlankLines(text) {
            return allLines = text.split('\n')
                .slice(0, content.maxResult)
                .filter(line => line.trim().length > 0 && !line.startsWith('='))
                .join(' ')                
                .replace(/\((?:\([^()]*\)|[^()])*\)/gm, '')
                .replace(/\s+/g,' ')
                
        }
        content.sourceContentSanitized = withoutBlankLinesAndMarkdown
        
    }

    function breakContentIntoSentence(content) {
        const sentences = sbd.sentences(content.sourceContentSanitized)
            .map(sentence => sentence = {
                text: sentence,
                keywords: [],
                images: []
            })

        content.sentences = sentences.slice(0, content.maxResult)
    }

    async function fetchKeywordsOfAllSentences(content) {
        console.log('> [text-robot] Starting to fetch keywords from watson')

        for (let sentence of content.sentences) {
            console.log(`> [text-robot] Sentence: "${sentence.text}"`)
            sentence.keywords = await fetchWatsonAndReturnKeywords(sentence.text)
            console.log(`> [text-robot] Keywords: ${sentence.keywords.join(', ')}\n`)
        }

    }

    async function fetchWatsonAndReturnKeywords(sentence) {
        return await nlu.analyze({
            text: sentence,
            features: {
                keywords: {}
            }
        })
        .then(resp => resp.keywords.map( keyword => keyword.text ))
        .catch(err => console.error(err))
    }

}

module.exports = robot