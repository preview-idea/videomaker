const readline = require('readline-sync')
const state = require('../robots/state')

// SETTINGS
const settings = {
    prefixs: ['Who is','What is','The history of'],
    langs: ['en','es','fr','pt'],
    results: [8,12,18,24]
}

function robot() {
    const content = {
        language: setLanguageForContent(),
        searchTerm: askAndReturnSearchTerm(),
        prefix: askAndReturnPrefix(),
        maxResult: askAndReturnMaxResult()
    }

    state.save(content)

    function setLanguageForContent() {
        return settings.langs[readline.keyInSelect(settings.langs, 'Choose one language: ')]
    }

    function askAndReturnSearchTerm() {
        return readline.question('Type a Wikipedia search term: ')
    }

    function askAndReturnPrefix() {
        return settings.prefixs[readline.keyInSelect(settings.prefixs, 'Choose one option: ')]
    }

    function askAndReturnMaxResult() {
        return settings.results[readline.keyInSelect(settings.results, 'Choose a max result: ')]
    }
}

module.exports = robot