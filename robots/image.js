const google = require('googleapis').google
const customSearch = google.customsearch('v1')
const googleSearchCredentials = require('../credentials/google-search.json')
const imageDownloader = require('image-downloader')
const blackList = require('../content/blacklist.json')
const state = require('./state')

async function robot () {
    const content = state.load()

    await fetchImagesOfAllSentences(content)
    await downloadAllimages(content)

    state.save(content)

    async function fetchImagesOfAllSentences(content) {
        
        for (let sentence of content.sentences) {
            const randomKeyword = Math.floor(Math.random() * sentence.keywords.length)
            const query = `${content.searchTerm} ${sentence.keywords[randomKeyword]}`

            console.log(`> [image-robot] Querying Google Images with: "${query}"`)

            sentence.images = await fetchGoogleAndReturnImagesLinks(query)
            sentence.googleSearchQuery = query
        }
    }

    async function fetchGoogleAndReturnImagesLinks(query) {
        const response = await customSearch.cse.list({
            auth: googleSearchCredentials.apiKey,
            cx: googleSearchCredentials.searchEngineId,
            q: query,
            searchType: 'image',
            num: 3
        })
        
        return response.data.items.map(item => item.link)
    }

    async function downloadAllimages(content) {
        content.downloadImages = []
        
        for (let sentenceIndex = 0; sentenceIndex < content.sentences.length; sentenceIndex++) {
            const images = content.sentences[sentenceIndex].images

            for (let imgIndex = 0; imgIndex < images.length; imgIndex++) {
                const imageUrl = images[imgIndex]

                try {

                    if (content.downloadImages.includes(imageUrl) || blackList.images.includes(imageUrl)) { 
                        throw new Error('Image already downloaded') 
                    }
                    
                    await downloadAndSaveImage(imageUrl, `${sentenceIndex}-original.png`) 
                    content.downloadImages.push(imageUrl)

                    console.log(`> [image-robot] [${sentenceIndex}][${imgIndex}] Image successfully downloaded: ${imageUrl}`)
                    break

                } catch (error) {
                    console.log(`> [image-robot] [${sentenceIndex}][${imgIndex}] Error (${imageUrl}): ${error}`)
                }
                
            } // for image
            
        } // for sentence

    }

    async function downloadAndSaveImage(url, filename) {
        return await imageDownloader.image({
            url: url,
            dest: `./content/${filename}`

        })
    }
 
}

module.exports = robot