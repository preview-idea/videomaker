const fs = require('fs')
const contentFilePath = './content.json'
const scriptFilePath = './content/after-effects-script.js'

function save(content) {
    const contentString = JSON.stringify(content)
    return fs.writeFileSync(contentFilePath, contentString)
}

function saveScript(content) {
    const contentString = `var content = ${JSON.stringify(content)}`
    return fs.writeFileSync(scriptFilePath, contentString)
}

function load() {
    const fileBuffer = fs.readFileSync(contentFilePath, 'utf-8')
    const contentJSON = JSON.parse(fileBuffer)

    return contentJSON
}

module.exports = {
    save,
    saveScript,
    load    
}