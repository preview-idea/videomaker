const gm = require('gm').subClass( { imageMagick: true })
const state = require('./state')
const spawn = require('child_process').spawn
const path = require('path')
const rootPath = path.resolve(__dirname, '..')


async function robot() {
    console.log('> [video-robot] Starting...')
    const content = state.load()
    
    await convertAllImages(content)
    await createAllSentenceImages(content)
    await createYouTubeThumbnail()
    await createAfterEffectsScript(content)
    await renderVideoWithAfterEffects()

    async function convertAllImages(content) {
        for (let sentenceIndex = 0; sentenceIndex < content.sentences.length; sentenceIndex++) {
            await convertImage(sentenceIndex)          
        }        
    }

    async function convertImage(sentenceIndex) {

        return new Promise((res, rej) => {
            const inputFile  = `./content/${sentenceIndex}-original.png`
            const outputFile = `./content/${sentenceIndex}-converted.png`
            const width = 1920
            const height = 1080

            gm()
                .in(inputFile)
                .out('(')
                    .out('-clone')
                    .out('0')
                    .out('-background', 'white')
                    .out('-blur', '0x9')
                    .out('-resize', `${width}x${height}^`)
                .out(')')
                .out('(')
                    .out('-clone')
                    .out('0')
                    .out('-background', 'white')
                    .out('-resize', `${width}x${height}`)
                .out(')')
                .out('-delete', '0')
                .out('-gravity', 'center')
                .out('-compose', 'over')
                .out('-composite')
                .out('-extent', `${width}x${height}`)
                .write(outputFile, (error) => {
                    if (error) { return rej(error) }

                    console.log(`> [video-robot] Image converted: ${outputFile}`)
                    res()

                    process.exit(0)
                })
        })
    }

    async function createAllSentenceImages(content) {
        for (let sentenceIndex = 0; sentenceIndex < content.sentences.length; sentenceIndex++) {
            await createSentenceImage(sentenceIndex, content.sentences[sentenceIndex].text)
        }       
    }

    async function createSentenceImage(sentenceIndex, sentenceText) {
        
        return new Promise((res, rej) => {
            const outputFile = `./content/${sentenceIndex}-sentence.png`

            const templateSettings = { 
                size: ['1920x400', '1024x760', '1920x800', '1920x200'], 
                gravity: ['center','west', 'east', 'south'] 
            }           
     
            gm()
                .out('-size', templateSettings.size[Math.floor(Math.random() * templateSettings.size.length)])
                .out('-gravity', templateSettings.gravity[Math.floor(Math.random() * templateSettings.gravity.length)])
                .out('-background', 'transparent')
                .out('-fill', 'white')
                .out('-kerning', '-1')
                .out(`caption:${sentenceText}`)
                .write(outputFile, (error) => {
                    if (error) { return rej(error) }

                    console.log(`> [video-robot] Sentence created: ${outputFile}`)
                    res()
                })
        })
    }

    async function createYouTubeThumbnail() {
        return new Promise((res, rej) => {
            gm()
                .in('./content/0-converted.png')
                .write('./content/youtube-thumbnail.jpg', (error) => {
                    if (error) { return rej(error) }

                    console.log('> [video-robot] YouTube thumbnail created')
                    res()
                })
        })
    }

    async function createAfterEffectsScript(content) {
        await state.saveScript(content)
    }

    async function renderVideoWithAfterEffects() {
        return new Promise((res, rej) => {
            const aerenderFilePath = '/Applications/Adobe After Effects CC 2019/aerender'
            const templateFilePath = `${rootPath}/templates/1/template.aep`
            const destinationFilePath = `${rootPath}/content/output.mov`

            console.log('> [video-robot] Starting After Effects')

            const aerender = spawn(aerenderFilePath, [
                '-comp', 'main',
                '-project', templateFilePath,
                '-output', destinationFilePath
            ])

            aerender.stdout.on('data', (data) => {
                process.stdout.write(data)
            })

            aerender.on('close', () => {
                console.log('> [video-robot] After Effects closed')
                res()
            })
        })
    }
    
}

module.exports = robot